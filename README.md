Место
=====

~~~
Все говорят, что мы в месте…
Все говорят, но немногие знают, в каком.
(c)
~~~

Для корректной работы требуется node v12.0 или выше.

Установка
---------

~~~
git clone git@bitbucket.org:backender76/yandex-praktikum-node.git
cd yandex-praktikum-node
npm i
~~~

NPM-скрипты
-----------

- `npm run frontend-build` - Сборка frontend'а
- `npm run frontend-dev` - Запуск frontend'а в режиме разработки
- `npm run frontend-deploy` - Деплой frontend'а на GitHub Pages
- `npm run backend-start` - Запуск backend'а в режиме production
- `npm run backend-dev` - Запуск backend'а в режиме разработки

API
---

### Список всех пользователей

- URL: `/users/`
- METHOD: `GET`
- Success Response:
  
  Status Code: `200`
  
  Content: 

    ~~~
    [
      {
        "name":"Ada Lovelace",
        "about":"Mathematician, writer",
        "avatar":"https://www.biography.com/.image/t_share/MTE4MDAzNDEwODQwOTQ2MTkw/ada-lovelace-20825279-1-402.jpg",
        "_id":"dbfe53c3c4d568240378b0c6"
      },
    ....
    ]
    ~~~

- Error Response:
  
  Status Code: `500`
  
  Content: `{ message: 'Что-то пошло не так' }`

### Пользователь по ID

- URL: `/users/:id`
- METHOD: `GET`

- Success Response:
  
  Status Code: `200`
  
  Content: 
  
    ~~~
    {
    "name":"Ada Lovelace",
    "about":"Mathematician, writer",
    "avatar":"https://www.biography.com/.image/t_share/MTE4MDAzNDEwODQwOTQ2MTkw/ada-lovelace-20825279-1-402.jpg",
    "_id":"dbfe53c3c4d568240378b0c6"
    }
    ~~~

- Error Response:
  
  Status Code: `500`
  
  Content: `{ message: 'Что-то пошло не так' }`

  Или

  Status Code: `404`
  
  Content: `{ message: 'Нет пользователя с таким id' }`

### Список карточек

- URL: `/cards/`
- METHOD: `GET`
- Success Response:
  
  Status Code: `200`
  
  Content: 
  
    ~~~
    [
      {
        "likes": [
          {
            "name": "Tim Berners-Lee",
            "about": "Inventor, scientist",
            "avatar": "https://media.wired.com/photos/5c86f3dd67bf5c2d3c382474/4:3/w_2400,h_1800,c_limit/TBL-RTX6HE9J-(1).jpg",
            "_id": "d285e3dceed844f902650f40"
          }
        ],
        "_id": "5d208fb50fdbbf001ffdf72a",
        "name": "Иваново",
        "link": "https://pictures.s3.yandex.net/frontend-developer/cards-compressed/ivanovo.jpg",
        "owner": {
          "name": "Bret Victor",
          "about": "Designer, engineer",
          "avatar": "https://postlight.com/wp-content/uploads/2018/03/109TC-e1535047852633.jpg",
          "_id": "8340d0ec33270a25f2413b69"
        },
        "createdAt": "2019-07-06T12:10:29.149Z"
      },
    ....
    ]
    ~~~

- Error Response:
  
  Status Code: `500`
  
  Content: `{ message: 'Что-то пошло не так' }`
