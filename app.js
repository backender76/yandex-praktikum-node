const express = require('express');
const path = require('path');
const config = require('config');

const WEB_SERVER_PORT = config.get('app.port');
const PATH_TO_PUBLIC = path.join(__dirname, '/public');

const app = express();

app.use(express.static(PATH_TO_PUBLIC));
app.use('/', require('./api/routes'));

app.listen(WEB_SERVER_PORT);
