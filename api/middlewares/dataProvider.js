const fs = require('fs');
const path = require('path');
const fsp = require('fs').promises;

const MS_IN_SECOND = 1000;

class DataProvider {
  constructor(pathToSource, cacheLifetime) {
    this.pathToSource = pathToSource;
    this.cacheLifetime = cacheLifetime || 5 * MS_IN_SECOND;
    this.timeSync = 0;
    this.promise = null;

    const dir = path.dirname(pathToSource);

    try {
      fs.accessSync(dir, fs.R_OK);
    } catch (e) {
      throw new Error(`Дирректория "${dir}" должна быть доступна для чтения`);
    }
    if (!fs.existsSync(pathToSource)) {
      throw new Error(`Файл "${pathToSource}" должен существовать`);
    }
    try {
      fs.accessSync(pathToSource, fs.R_OK);
    } catch (e) {
      throw new Error(`Файл "${pathToSource}" должен быть доступен для чтения`);
    }
  }

  get() {
    if (!this.promise || this.isCacheExpired()) {
      this.promise = fsp.readFile(this.pathToSource, 'utf8')
        .then((content) => {
          this.timeSync = Date.now();
          return [JSON.parse(content), null];
        })
        .catch((err) => [null, err]);
    }
    return this.promise;
  }

  isCacheExpired() {
    return Date.now() - this.timeSync > this.cacheLifetime;
  }
}

module.exports = (pathToSource, cacheLifetime) => {
  const dataProvider = new DataProvider(pathToSource, cacheLifetime);

  return (req, res, next) => {
    req.dataProvider = dataProvider;
    next();
  };
};
