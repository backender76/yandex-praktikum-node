const router = require('express-promise-router')();
const path = require('path');
const config = require('config');

const REL_PATH_TO_DATA = config.get('usersDataProvider.path');
const ABS_PATH_TO_DATA = path.join(__dirname, '../../..', REL_PATH_TO_DATA);
const CACHE_LIFETIME = config.get('usersDataProvider.cacheLifetime');

const dataProvider = require('../../middlewares/dataProvider');

router.use(dataProvider(ABS_PATH_TO_DATA, CACHE_LIFETIME));
router.get('/', require('./get-all'));
router.get('/:userId', require('./get'));

module.exports = router;
