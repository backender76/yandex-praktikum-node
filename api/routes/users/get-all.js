module.exports = async (req, res) => {
  const [users, error] = await req.dataProvider.get();

  if (error) {
    return res.status(500).json({ message: 'Что-то пошло не так' });
  }
  return res.json(users);
};
