module.exports = async (req, res) => {
  const { userId } = req.params;
  const [users, error] = await req.dataProvider.get();

  if (error) {
    return res.status(500).json({ message: 'Что-то пошло не так' });
  }
  const user = users.find((u) => u._id === userId);

  if (!user) {
    return res.status(404).json({ message: 'Нет пользователя с таким id' });
  }
  return res.json(user);
};
