const router = require('express-promise-router')();

router.use('/users', require('./users'));
router.use('/cards', require('./cards'));

router.use((req, res) => res.status(404).json({ message: 'Запрашиваемый ресурс не найден' }));

module.exports = router;
