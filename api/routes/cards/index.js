const router = require('express-promise-router')();
const path = require('path');
const config = require('config');

const REL_PATH_TO_DATA = config.get('cardsDataProvider.path');
const ABS_PATH_TO_DATA = path.join(__dirname, '../../..', REL_PATH_TO_DATA);
const CACHE_LIFETIME = config.get('cardsDataProvider.cacheLifetime');

const dataProvider = require('../../middlewares/dataProvider');

router.use(dataProvider(ABS_PATH_TO_DATA, CACHE_LIFETIME));
router.get('/', require('./get-all'));

module.exports = router;
